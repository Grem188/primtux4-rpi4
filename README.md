**Script de construction de PrimTux4-Debian10-CTP sur Raspberry Pi**

Ce script permet de construire une distribution PrimTux4-Debian10-CTP pour le nano ordinateur Raspberry Pi sur la base d'une Raspbian lite Buster.

*IMPORTANT : une carte 16 Go est insuffisante pour cette construction.*
 
## Utilisation :

Le Raspberry Pi doit être démarré avec une carte SD sur laquelle est installée une Raspbian Lite Buster.

Connectez-vous avec le login par défaut:

```
pi
```

Mot de passe par défaut de Raspbian:

```
raspberry
```

ATTENTION: par défaut Raspbian est configuré avec un clavier anglo-saxon. Il faudra en tenir compte lors des saisies. Pour le mot de passe, avec un clavier AZERTY, il faut saisir `rqspberry`.

Configurez les paramètres de localisation, de clavier, de WiFi à l'aide de l'utilitaire inclus raspi-config

```
sudo raspi-config
```

(sudo `rqspi)config` avec un clavier AZERTY)

Activez le compte root en lui attribuant un mot de passe:

```
sudo passwd root
```

Je vous invite à saisir `tuxprof` comme mot de passe, car c'est celui proposé par défaut dans PrimTux. Vous pourrez toujours le changer par la suite.

Redémarrez puis connectez-vous sous le compte root.

```
wget https://www.primtux.fr/Documentation/armhf/install-ptx4-rpi4.sh

chmod +x install-ptx4-rpi4.sh

./install-ptx4-rpi4.sh
```

L'opération nécessite plusieurs heures, et dépend de la qualité de votre liaison Internet.

L'installation de Samba et Contrôle Parental se fait de façon interactive et nécessite de répondre à quelques questions de configuration. Il vous sera également demandé si vous souhaitez ou non reconfigurer le clavier.

Redémarrez en fin d'opération. 

Le script crée un fichier log des erreurs en `/var/log/install-primtux-rpi.log`
