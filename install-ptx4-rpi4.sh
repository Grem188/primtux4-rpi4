#!/bin/bash

########################################
# Script de construction d'une PrimTux #
# sur Raspberry Pi disposant d'une     #
# Raspbian lite installée.             #
# Auteur: Philippe Ronflette           #
# philippe.dpt35@yahoo.fr              #
########################################

debut=$(date +%s)

# Paramétrage des sources, à modifier si nécessaire
sources="https://framagit.org/Steph/ptx4/raw/master/armhf/sources/includes.chroot.tar https://framagit.org/Steph/ptx4/raw/master/armhf/sources/hooks.tar"
archive1="includes.chroot.tar"
archive2="hooks.tar"
config_pcmanfm="https://www.primtux.fr/Documentation/armhf/pcmanfm.conf.tar"
config_libfm="https://www.primtux.fr/Documentation/armhf/libfm.conf.tar"
ctparental="https://www.primtux.fr/Documentation/armhf/ctparental-rpi4.deb"
config_ctparental="https://www.primtux.fr/Documentation/armhf/ctparental.conf.ptx4.rpi.tar.gz"
icone_gspeech="https://www.primtux.fr/Documentation/armhf/gspeech.png"

version="PrimTux4 Debian10 RPi CTP"

# Listes des paquets à installer
paquets_base="xorg fluxbox lightdm lxappearance lxpanel xfce4-panel roxterm leafpad spacefm pcmanfm menulibre rox-filer gvfs samba synaptic gdebi firefox-esr nginx"
paquets_polices="xfonts-100dpi xfonts-75dpi xfonts-base xfonts-terminus fonts-droid-fallback fonts-liberation gnome-font-viewer fonts-opendyslexic fonts-dejavu-core fonts-dejavu-extra fonts-liberation fonts-sil-gentium fonts-sil-gentium-basic gnome-icon-theme-symbolic ttf-dejavu-core fonts-wine"
paquets_themes="gtk2-engines-murrine murrine-themes adwaita-icon-theme faenza-icon-theme gtk2-engines gtk2-engines-pixbuf"
paquets_wifi="bluetooth wicd gnome-bluetooth"
paquets_divers="accountsservice alsa-tools anacron arandr audacity blobby busybox childsplay cpulimit dillo disk-manager dkms dnsmasq dnsmasq-base dnsutils file-roller firefox-esr-l10n-fr fonts-sil-andika fotowall frozen-bubble fusesmb fxload gamin gcompris geany gigolo gnome-calculator gnome-mplayer gnome-system-tools gnucap gnupg2 goldendict gstreamer0.10-plugins-base gstreamer1.0-plugins-good gstreamer1.0-pulseaudio gstreamer0.10-tools gstreamer1.0-fluendo-mp3 gstreamer1.0-packagekit gstreamer1.0-plugins-ugly gtans gtk3-engines-unico gxmessage hannah hardinfo htop iwidgets4 jclic jmtpfs katepart klettres ktuberling libsexy2 libtk-img libttspico-utils mirage mlocate monsterz mtp-tools musescore numlockx openjdk-8-jre openshot osmo pavucontrol php7.3 pinta pimixer pm-utils powermgmt-base pulseaudio pysycache pysycache-buttons-beerabbit pysycache-buttons-crapaud pysycache-buttons-wolf pysycache-click-sea pysycache-dblclick-butterfly pysycache-move-food pysycache-move-plants pysycache-move-sky pysycache-move-sports pysycache-puzzle-photos python-glade2 python-yaml python-webkit qalculate-gtk ri-li ruby scratch seahorse seahorse-adventures flameshot soundconverter stellarium supertux synapse tcl tcl8.4 tcl8.5 tk8.5 tk8.6 trash-cli tuxmath tuxpaint udevil user-setup vim-runtime whois winff xapps-common xfburn xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-fsguard-plugin xfce4-indicator-plugin xfce4-netload-plugin xfce4-quicklauncher-plugin xfce4-systemload-plugin xournal xpdf xsane xscreensaver zenity xfce4-notifyd"
paquets_primtux="accueil-primtux2 administration-eleves-primtux arreter-primtux autologin-primtux-ubuntu documentation-primtux drgeo-raspbian fskbsetting geonext-primtux geotortue-stretch geotortue-stretch gtkdialog handymenu intef-exe jnavigue-primtux leterrier-calculette-capricieuse leterrier-calculment leterrier-cibler leterrier-fubuki leterrier-imageo leterrier-suitearithmetique leterrier-tierce lightdm-webkit-greeter microscope-virtual-primtux mothsart-wallpapers-primtux multiplication-station-primtux omnitux-light pysiogame python-sexy sauve-carte tbo toutenclic wordsearchcreator"
dependances_ctparental="console-data libnftables0 libnss3-tools nftables php-cgi php-fpm php-xml php7.3-cgi php7.3-fpm php7.3-xml"

# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/install-primtux-rpi.log"
if ! [ -e "$fichierlog" ]
    then > "$fichierlog"
fi
date >> "$fichierlog"
exec 2>>"$fichierlog"

# Ajout des dépôts PrimTux
touch /etc/apt/sources.list.d/primtux.list
if ! grep -e "^deb https://mirrors.o2switch.fr/primtux/repo/debs/ PrimTux-armhf main" /etc/apt/sources.list.d/primtux.list >/dev/null
   then echo "deb https://mirrors.o2switch.fr/primtux/repo/debs/ PrimTux-armhf main" >> /etc/apt/sources.list.d/primtux.list
fi
if ! grep -e "^deb https://mirrors.o2switch.fr/primtux/repo/debs/ PrimTux4-armhf PrimTux-armhf main" /etc/apt/sources.list.d/primtux.list >/dev/null
   then echo "deb https://mirrors.o2switch.fr/primtux/repo/debs/ PrimTux4-armhf PrimTux-armhf main" >> /etc/apt/sources.list.d/primtux.list
fi

# Ajout de la clé publique du dépôt
wget -O - https://depot.primtux.fr/key/PrimTux.gpg.key | apt-key add -

# Ajout du dépôt non libre pour libttspico-utils
wget -q https://ftp-master.debian.org/keys/release-10.asc -O- | apt-key add -
if ! grep -e "^deb http://deb.debian.org/debian buster non-free" /etc/apt/sources.list >/dev/null
   then echo "deb http://deb.debian.org/debian buster non-free" >> /etc/apt/sources.list
fi

apt update
apt upgrade -y

# Paquets de base
for paquet in ${paquets_base}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done

# Paquets de la distribution PrimTux
for paquet in ${paquets_polices}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done
for paquet in ${paquets_themes}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done
for paquet in ${paquets_wifi}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done
for paquet in ${paquets_divers}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done

#Pour résoudre les problèmes avec lightdm
apt install --no-install-recommends --no-install-suggests -y libwebkitgtk-3.0-0

# Installation des paquets du dépôt PrimTux
for paquet in ${paquets_primtux}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done

# Installation des dépendances de CTparental
for paquet in ${dependances_ctparental}
do
   apt install --no-install-recommends --no-install-suggests -y "$paquet"
done

# Pour les conflits Internet et WiFi
apt purge -y dhcpcd5 network-manager network-manager-gnome
apt autoremove -y

apt --fix-broken install -y

# Installation de log2ram
wget https://github.com/azlux/log2ram/archive/master.tar.gz
tar xf master.tar.gz
rm master.tar.gz
cd log2ram-master
chmod +x install.sh
./install.sh
cd ..
rm -r log2ram-master

# Récupération des sources de PrimTux 4
mkdir /tmp/sources-primtux
wget -P /tmp/sources-primtux $sources
tar xvf /tmp/sources-primtux/"$archive1" -C /tmp/sources-primtux
tar xvf /tmp/sources-primtux/"$archive2" -C /tmp/sources-primtux
rm /tmp/sources-primtux/"$archive1" /tmp/sources-primtux/"$archive2"

# Copie les fichiers sur le système
rsync -av /tmp/sources-primtux/includes.chroot/* /
# Remplace les fichiers de configuration du gestionnaire de fichiers par ceux pour RPi
wget -P /tmp "$config_pcmanfm"
wget -P /tmp "$config_libfm"
tar xvf /tmp/$(basename "$config_pcmanfm") -C /etc/skel-mini/.config/pcmanfm/default
tar xvf /tmp/$(basename "$config_libfm") -C /etc/skel-mini/.config/libfm
tar xvf /tmp/$(basename "$config_pcmanfm") -C /etc/skel-super/.config/pcmanfm/default
tar xvf /tmp/$(basename "$config_libfm") -C /etc/skel-super/.config/libfm
tar xvf /tmp/$(basename "$config_pcmanfm") -C /etc/skel-maxi/.config/pcmanfm/default
tar xvf /tmp/$(basename "$config_libfm") -C /etc/skel-maxi/.config/libfm
rm /tmp/$(basename "$config_pcmanfm")
rm /tmp/$(basename "$config_libfm")

# Applications des scripts de configuration de PrimTux
sh /tmp/sources-primtux/hooks/normal/1000.useradd.hook.chroot
# Copie les répertoires de chaque utilisateur
rsync -av /etc/skel/ /home/administrateur
rsync -av /etc/skel-mini/ /home/01-mini
rsync -av /etc/skel-super/ /home/02-super
rsync -av /etc/skel-maxi/ /home/03-maxi

# on donne à chaque répertoire utilisateur ses droits
chown -R 01-mini:01-mini /home/01-mini
chown -R 02-super:02-super /home/02-super
chown -R 03-maxi:03-maxi /home/03-maxi
chown -R administrateur:administrateur /home/administrateur

sh /tmp/sources-primtux/hooks/normal/1001.chown-rep.hook.chroot
sh /tmp/sources-primtux/hooks/normal/1003.bash-false.hook.chroot
sh /tmp/sources-primtux/hooks/normal/1004.rep-public.hook.chroot

# Désactivation des shell élèves
chsh -s /bin/rbash 01-mini
chsh -s /bin/rbash 02-super
chsh -s /bin/rbash 03-maxi
mv /bin/rbash /bin/rbash.copie
cp /bin/false /bin/rbash

# Suppression des répertoires de calcul@TICE pour l'instant non intégré
rm -rf /home/{01-mini,02-super,03-maxi,administrateur}/clc-linux

# Accès au réseau local
sed -i '/workgroup = WORKGROUP/ a client max protocol = NT1' /etc/samba/smb.conf

# Installation de ctparental
wget -P /var/cache/apt/archives/ "$ctparental"
wget -P /tmp "$config_ctparental"
dpkg -i /var/cache/apt/archives/$(basename "$ctparental")
rm /etc/resolv.conf
echo "nameserver 1.1.1.1" > /etc/resolv.conf
chattr +i /etc/resolv.conf
tar xvf /tmp/$(basename "$config_ctparental") -C /
rm /tmp/$(basename "$config_ctparental")
rm /usr/share/primtux/ctparental*.* /usr/share/primtux/CTparental*.*

apt --fix-broken install -y

# On nettoie !
rm -rf /tmp/sources-primtux

# Correction du nom différent pour l'application calcul mental
ln -s /usr/bin/leterrier-calculment /usr/bin/leterrier-calcul-mental

# Icone Gspeech
wget "$icone_gspeech"
cp gspeech.png /usr/share/pixmaps/gspeech.png

# Vérification des paquets installés
paquets=$(echo "$paquets_base $paquets_polices $paquets_themes $paquets_wifi $paquets_divers $paquets_primtux")
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > /tmp/installes.txt
echo "Paquets manquants :" >> "$fichierlog"
for paquet in ${paquets}
do
   if ! grep "$paquet" /tmp/installes.txt > /dev/null; then
      echo "$paquet" >> "$fichierlog"
   fi
done
rm /tmp/installes.txt

# Indication de version de l'OS
echo "$version" >/etc/primtux_version

# Nettoyage du fichier des erreurs
sed -i '/[1-9].*K \.\.\.\.*/ d' "$fichierlog"
fin=$(date +%s)
temps=$(($fin-$debut))
temps=$(echo $temps |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "
Les opérations sont terminées et ont duré $temps
Un fichier des erreurs a été créé en /var/log/install-primtux-rpi.log
Veuillez redémarrer le système."
echo "
#######################################################################
" >> "$fichierlog"
exit 0
